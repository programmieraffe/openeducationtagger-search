
import React from 'react';
import ReactDOM from 'react-dom';

import {
	ReactiveBase,
	DataSearch,
	MultiList,
	SelectedFilters,
	ReactiveList
} from '@appbaseio/reactivesearch';
import {
	Row,
	Button,
	Col,
	Card,
	Switch,
	Tree,
	Popover,
	Affix
} from 'antd';
import 'antd/dist/antd.css';


function getNestedValue(obj, path) {
	console.log('gestNestedValue obj,path',obj,path);
	const keys = path.split('.');
	const currentObject = obj;
	const nestedValue = keys.reduce((value, key) => {
		if (value) {
		return value[key];
		}
		return '';
	}, currentObject);
	if (typeof nestedValue === 'object') {
		return JSON.stringify(nestedValue);
	}
	return nestedValue;
}

function renderItem(res, triggerClickAnalytics) {
	let { image, url, description, title } = {"title":"name","description":"description","image":"","url":"url","showRest":true};
	image = getNestedValue(res,image);
	title = getNestedValue(res,title);
	url = getNestedValue(res,url);
	description = getNestedValue(res,description)
	return (
		<Row onClick={triggerClickAnalytics} type="flex" gutter={16} key={res._id} style={{margin:'20px auto',borderBottom:'1px solid #ededed'}}>
			<Col span={image ? 6 : 0}>
				{image &&  <img src={image} alt={title} /> }
			</Col>
			<Col span={image ? 18 : 24}>
				<h3 style={{ fontWeight: '600' }} dangerouslySetInnerHTML={{__html: title || 'Choose a valid Title Field'}}/>
				<p style={{ fontSize: '1em' }} dangerouslySetInnerHTML={{__html: description || 'Choose a valid Description Field'}}/>
			</Col>
			<div style={{padding:'20px'}}>
				{url ? <Button shape="circle" icon="link" style={{ marginRight: '5px' }} onClick={() => window.open(url, '_blank')} />
: null}
			</div>
		</Row>
	);
};

// 2DO: I'm sure there is a better way to achieve this ;-)
function getUrl() {
	const search = window.location.search;
	const params = new URLSearchParams(search);
	//console.log('esurl',typeof params.get('esurl'));
	if(params.get('url') == null){
		return process.env.REACT_APP_ELASTICSEARCH_URL;
	}else{
			return params.get('url');
	}
};

function getCredentials(){
	const search = window.location.search;
	const params = new URLSearchParams(search);
	//console.log('esurl',typeof params.get('esurl'));
	if(params.get('auth') == null){
		return 	process.env.REACT_APP_ELASTICSEARCH_AUTH_STRING_READ;
	}else{
			return params.get('auth');
	}
}

function getIndexName(){
	const search = window.location.search;
	const params = new URLSearchParams(search);
	console.log('esurl',params.get('index'));
	if(params.get('index') == null){
		console.log('return it',process.env.REACT_APP_ELASTICSEARCH_INDEXNAME);
		return process.env.REACT_APP_ELASTICSEARCH_INDEXNAME;
	}else{
			return params.get('index');
	}
}

const App = () => (
	<ReactiveBase
		app={getIndexName()}
		// {process.env.REACT_APP_ELASTICSEARCH_INDEXNAME}
		credentials={getCredentials()}
		//credentials={process.env.REACT_APP_ELASTICSEARCH_AUTH_STRING_READ}
		url={getUrl()}
		// {process.env.REACT_APP_ELASTICSEARCH_URL}
		analytics={false}
		searchStateHeader
		>
		<Row gutter={16} style={{ padding: 10 }}>
			<Col span={16}>
				<h1>{process.env.REACT_APP_TITLE}</h1>
				<p className="description">{process.env.REACT_APP_DESCRIPTION}</p>
			</Col>
		</Row>
		<Row gutter={16} style={{ padding: 10 }}>
			<Col span={8}>
				<Card>
				<MultiList
				  componentId="list-1"
				  dataField="subjectArea.keyword"
				  size={100}
					URLParams={true}
				  style={{
				    marginBottom: 20
				  }}
				  title="Subject area"
				/>
				<MultiList
					componentId="list-6"
					dataField="creator.name.keyword"
					size={100}
					showSearch={true}
					style={{
						marginBottom: 20
					}}
					title="Creator"
				/>
				<MultiList
				  componentId="list-2"
				  dataField="learningResourceType.keyword"
				  showCheckbox={true}
				  size={100}
				  style={{
				    marginBottom: 20
				  }}
				  title="Types"
				/>
				<MultiList
				  componentId="list-3"
				  dataField="tag.keyword"
				  size={100}
				  style={{
				    marginBottom: 20
				  }}
				  title="Tags"
				/>
				<MultiList
				  componentId="list-4"
				  dataField="inLanguage.keyword"
				  size={100}
				  style={{
				    marginBottom: 20
				  }}
				  title="Language"
				/>
				<MultiList
				  componentId="list-5"
				  dataField="licenseType.keyword"
				  size={100}
					showSearch={false}
				  style={{
				    marginBottom: 20
				  }}
				  title="License"
				/>
				</Card>
			</Col>
			<Col span={12}>
				<DataSearch
				  componentId="search"
				  dataField={[
				    'description','name','tag','learningResourceType','subjectAreas','creator'
				  ]}
				  fieldWeights={[
				    1
				  ]}
				  fuzziness={0}
				  highlightField={[
				    'description'
				  ]}
				  style={{
				    marginBottom: 20
				  }}
				/>

				<SelectedFilters />
				<div id="result">
					<ReactiveList
				  componentId="result"
				  dataField="_score"
				  pagination={true}
				  react={{
				    and: [
				      'list-1',
				      'list-2',
				      'list-3',
				      'list-4',
							'list-5',
							'list-6',
				      'search'
				    ]
				  }}
				  renderItem={renderItem}
				  size={5}
				  style={{
				    marginTop: 20
				  }}
				/>
				</div>
			</Col>

		</Row>
	</ReactiveBase>
);

ReactDOM.render(
	<App />,
	document.getElementById('root')
);

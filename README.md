
# Open Education Tagger

This is the repository of the generalized search UI of OpenEducationTagger.

Find full documentation here:
https://github.com/programmieraffe/openeducationtagger

Live example:
https://programmieraffe.gitlab.io/openeducationtagger-search/?url=https://coronacampus-691643726.eu-central-1.bonsaisearch.net&index=index1&auth=qjoZXnydFC:gPY6fNtC5VTDBbXahckQ

Usage with GET-parameters:
https://programmieraffe.gitlab.io/openeducationtagger-search/?url=YOURELASTICURL&index=INDEXNAME&auth=AUTHSTRINGREAD

## Developer notes

### Setup with Gitlab CI + Gitlab pages

1. Fork
2. (Remove fork connection in settings)
3. Edit `.env` and add config values for your elasticsearch instance
5. Check "CI/CD" -> "Jobs", project build is triggered
6. Open it in browser (URL available in "Settings" -> "Pages")

### Local testing

1. git clone to local drive
2. `cd` into directory
2. `yarn install`
3. `yarn start`

- You can use `.env.local` to use local values (should override `.env` production config.

### 2DO

- [change gitlab ci to yarn?](https://gist.github.com/superjose/709989dd58aa90bfeda75767668482b2)
- change css framework to bootstrap?

### How this was developed

1. Create github/gitlab repository & git clone
2. npx create-react-app REPOFOLDER-NAME
3. cd REPOFOLDER-NAME
3. `yarn add @appbaseio/reactivesearch` (or npm install)
4. Documentation: [reactivesearch-Quickstart](https://docs.appbase.io/docs/reactivesearch/gettingstarted/)

yarn add antd

Logo/Favicon: Emoji designed by [OpenMoji](https://openmoji.org/) – the open-source emoji and icon project. License: [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/#)
